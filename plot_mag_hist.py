import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

rcParams.update({'font.size': 32})
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

#-------------------------

#in_event_mag_file = '../data/20190704_73hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20190704_73hr_magnitudes,\n MATCH from scedc' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_MATCH_scedc_gpdbinder_from_scedc.pdf'

#in_event_mag_file = '../data/20190704_73hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20190704_73hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_MATCH_scedc_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20190704_73hr_events_MISSED_scedc_gpdbinder.txt'
#in_plot_title = '20190704_73hr_magnitudes,\n MISSED from scedc' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_MISSED_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20190704_73hr_events_NEW_scedc_gpdbinder.txt'
#in_plot_title = '20190704_73hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_NEW_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20190704_73hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20190704_73hr_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20190704_73hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20190704_73hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20190704_73hr_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20190704_73hr_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_MISSED_pickew_gpdbinder.pdf'

in_event_mag_file = '../data/20190704_73hr_events_NEW_pickew_gpdbinder.txt'
in_plot_title = '20190704_73hr_magnitudes,\n NEW from gpdbinder' 
ind_mag_col = 5
out_plot_magfreq_file = '../figs/20190704_73hr_magnitudes_NEW_pickew_gpdbinder.pdf'

#-------------------------

#in_event_mag_file = '../data/20200930_1545hr_events_MATCH_comcat_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MATCH from comcat' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MATCH_comcat_gpdbinder_from_comcat.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MATCH_comcat_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MATCH_comcat_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MISSED_comcat_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MISSED from comcat' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MISSED_comcat_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_NEW_comcat_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_NEW_comcat_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MATCH_comcat_pickew.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MATCH from comcat' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MATCH_comcat_pickew_from_comcat.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MATCH_comcat_pickew.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MATCH_comcat_pickew_from_pickew.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MISSED_comcat_pickew.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MISSED from comcat' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MISSED_comcat_pickew.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MISSED_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_NEW_pickew_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_NEW_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20200930_1545hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20200930_1545hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_1545hr_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#-------------------------

#in_event_mag_file = '../data/20200930_24hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n MATCH from scedc' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_MATCH_scedc_gpdbinder_from_scedc.pdf'

#in_event_mag_file = '../data/20200930_24hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_MATCH_scedc_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_24hr_events_MISSED_scedc_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n MISSED from scedc' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_MISSED_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_24hr_events_NEW_scedc_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_NEW_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_24hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20200930_24hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_24hr_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_MISSED_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20200930_24hr_events_NEW_pickew_gpdbinder.txt'
#in_plot_title = '20200930_24hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20200930_24hr_magnitudes_NEW_pickew_gpdbinder.pdf'

#-------------------------

#in_event_mag_file = '../data/20210104_24hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n MATCH from scedc' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_MATCH_scedc_gpdbinder_from_scedc.pdf'

#in_event_mag_file = '../data/20210104_24hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_MATCH_scedc_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20210104_24hr_events_MISSED_scedc_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n MISSED from scedc' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_MISSED_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20210104_24hr_events_NEW_scedc_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_NEW_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20210104_24hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20210104_24hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20210104_24hr_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_MISSED_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20210104_24hr_events_NEW_pickew_gpdbinder.txt'
#in_plot_title = '20210104_24hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210104_24hr_magnitudes_NEW_pickew_gpdbinder.pdf'

#-------------------------

#in_event_mag_file = '../data/20210605_24hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n MATCH from scedc' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_MATCH_scedc_gpdbinder_from_scedc.pdf'

#in_event_mag_file = '../data/20210605_24hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_MATCH_scedc_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20210605_24hr_events_MISSED_scedc_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n MISSED from scedc' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_MISSED_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20210605_24hr_events_NEW_scedc_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_NEW_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20210605_24hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20210605_24hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20210605_24hr_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_MISSED_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20210605_24hr_events_NEW_pickew_gpdbinder.txt'
#in_plot_title = '20210605_24hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20210605_24hr_magnitudes_NEW_pickew_gpdbinder.pdf'

#-------------------------

#in_event_mag_file = '../data/20220325_25hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n MATCH from scedc' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_MATCH_scedc_gpdbinder_from_scedc.pdf'

#in_event_mag_file = '../data/20220325_25hr_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_MATCH_scedc_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20220325_25hr_events_MISSED_scedc_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n MISSED from scedc' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_MISSED_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20220325_25hr_events_NEW_scedc_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_NEW_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20220325_25hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20220325_25hr_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20220325_25hr_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_MISSED_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20220325_25hr_events_NEW_pickew_gpdbinder.txt'
#in_plot_title = '20220325_25hr_magnitudes,\n NEW from gpdbinder'
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325_25hr_magnitudes_NEW_pickew_gpdbinder.pdf'

#-------------------------

#in_event_mag_file = '../data/20220325R_25hr_events_MATCH_replay_gpdbinder.txt'
#in_plot_title = '20220325R_25hr_magnitudes,\n MATCH from realtime'
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20220325R_25hr_magnitudes_MATCH_replay_gpdbinder_from_realtime.pdf'

#in_event_mag_file = '../data/20220325R_25hr_events_MATCH_replay_gpdbinder.txt'
#in_plot_title = '20220325R_25hr_magnitudes,\n MATCH from replay'
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325R_25hr_magnitudes_MATCH_replay_gpdbinder_from_replay.pdf'

#in_event_mag_file = '../data/20220325R_25hr_events_MISSED_replay_gpdbinder.txt'
#in_plot_title = '20220325R_25hr_magnitudes,\n MISSED from realtime'
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325R_25hr_magnitudes_MISSED_replay_realtime.pdf'

#in_event_mag_file = '../data/20220325R_25hr_events_NEW_replay_gpdbinder.txt'
#in_plot_title = '20220325R_25hr_magnitudes,\n NEW from replay'
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220325R_25hr_magnitudes_NEW_replay_replay.pdf'

#-------------------------

#in_event_mag_file = '../data/20220408_4d_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n MATCH from scedc' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_MATCH_scedc_gpdbinder_from_scedc.pdf'

#in_event_mag_file = '../data/20220408_4d_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_MATCH_scedc_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20220408_4d_events_MISSED_scedc_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n MISSED from scedc' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_MISSED_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20220408_4d_events_NEW_scedc_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_NEW_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20220408_4d_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20220408_4d_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20220408_4d_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_MISSED_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20220408_4d_events_NEW_pickew_gpdbinder.txt'
#in_plot_title = '20220408_4d_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220408_4d_magnitudes_NEW_pickew_gpdbinder.pdf'

#-------------------------

#in_event_mag_file = '../data/20220421_8d_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n MATCH from scedc' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_MATCH_scedc_gpdbinder_from_scedc.pdf'

#in_event_mag_file = '../data/20220421_8d_events_MATCH_scedc_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_MATCH_scedc_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20220421_8d_events_MISSED_scedc_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n MISSED from scedc' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_MISSED_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20220421_8d_events_NEW_scedc_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_NEW_scedc_gpdbinder.pdf'

#in_event_mag_file = '../data/20220421_8d_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n MATCH from pickew' 
#ind_mag_col = 11
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_MATCH_pickew_gpdbinder_from_pickew.pdf'

#in_event_mag_file = '../data/20220421_8d_events_MATCH_pickew_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n MATCH from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_MATCH_pickew_gpdbinder_from_gpdbinder.pdf'

#in_event_mag_file = '../data/20220421_8d_events_MISSED_pickew_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n MISSED from pickew' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_MISSED_pickew_gpdbinder.pdf'

#in_event_mag_file = '../data/20220421_8d_events_NEW_pickew_gpdbinder.txt'
#in_plot_title = '20220421_8d_magnitudes,\n NEW from gpdbinder' 
#ind_mag_col = 5
#out_plot_magfreq_file = '../figs/20220421_8d_magnitudes_NEW_pickew_gpdbinder.pdf'



[ev_ot, ev_mag] = np.genfromtxt(in_event_mag_file, dtype='str,float', usecols=(0,ind_mag_col), unpack=True)
print("len(ev_mag) = ", len(ev_mag))

inbins = np.arange(-0.95, 8.05, 0.1)
plt.figure(num=1, figsize=(10,8))
plt.clf()
n, bins, patches = plt.hist([ev_mag], bins=inbins, label=[str(len(ev_mag))+' events'], color=['yellow'], edgecolor='black', linewidth=0.5, stacked=True)
plt.xlim([-1, 8]) # Ridgecrest
#plt.xlim([-1, 6]) # Calipatria, Westmorland
#plt.xlim([-1, 4])
#plt.yscale('log', nonpositive='clip')
#   plt.ylim([0.1, 1000])
#plt.ylim([0.1, 100000])
#plt.ylim([0, 20])
#plt.ylim([0, 30])
#plt.ylim([0, 80]) # Calipatria
plt.ylim([0, 200]) # Ridgecrest
plt.xlabel("Magnitude")
plt.ylabel("Number of events")
plt.title(in_plot_title, pad=10)
plt.legend(prop={'size':32})
plt.tight_layout()
plt.savefig(out_plot_magfreq_file)
