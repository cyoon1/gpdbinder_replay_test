#!/bin/bash

# Script to get top line of eventhist
# Runs on amosite

# get_topline_aqms_event.sh
if [[ $# -eq 0 ]] ; then
   echo 'Usage: ./get_topline_aqms_event.sh <eventid>'
   exit 1
fi

# Get top line of eventhist; is it from a trigger (contain "H  RT")?
eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | head -n 1

