#!/bin/bash

# Script to get second line for AQMS automatic trigger event
# Runs on amosite

# get_second_aqms_trigger_event.sh
if [[ $# -eq 0 ]] ; then
   echo 'Usage: ./get_second_aqms_trigger_event.sh <eventid>'
   exit 1
fi

# Get second line (first location solution) for aqms trigger event (real-time, binder, original event solution)
#eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | sort -t',' -nk1,3 | awk '{$1=""; print $0}' | head -n 2 | tail -n 1
eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | sort -t',' -nk1,3 | head -n 2 | tail -n 1
