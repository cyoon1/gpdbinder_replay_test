#!/bin/bash

# Script to output AQMS PickEW event lists - separated by RT(event), RT(trigger), PP(Jiggle) - first automatic solution
# Runs on amosite - calls other scripts
#  get_topline_aqms_event.sh
#  get_second_aqms_trigger_event.sh
#  get_first_aqms_event.sh
#  get_sortedfirst_aqms_event.sh

# output_aqms_pickew_list.sh
if [[ $# -ne 3 ]] ; then
   echo 'Usage: ./output_aqms_pickew_list.sh <data_folder_str> <start_datetime> <end_datetime>'
   echo 'Example: ./output_aqms_pickew_list.sh 20210605_24hr 2021-06-05,00:00 2021-06-06,00:00' 
   exit 1
fi

# First create the output directory for this data set
DATA_FOLDER=$1
echo ${DATA_FOLDER}
mkdir ${DATA_FOLDER}
cd ${DATA_FOLDER}

# Check start and end datetime strings
START_TIME=$2
END_TIME=$3
echo ${START_TIME}
echo ${END_TIME}

# Get list of all event IDs between input START_TIME and END_TIME, then save the top line from eventhist with get_topline_aqms_event.sh
cattail -d archdbe ${START_TIME} ${END_TIME} -2 | awk '{if ($1 != "#" && $1 != "") print $1}' | xargs -I{} -d'\n' ../get_topline_aqms_event.sh {} > ${DATA_FOLDER}_toplines.txt 

# From toplines file, get list of events that started from RT subnet triggers
awk '/RT/' ${DATA_FOLDER}_toplines.txt > ${DATA_FOLDER}_toplines_from_triggers.txt

# Final RT(trigger) output: Get first event origins (from Jiggle) for events that started from RT subnet triggers with get_second_aqms_trigger_event.sh
cat ${DATA_FOLDER}_toplines_from_triggers.txt | awk -F'[,| ]' '{print $1}' | xargs -I{} -d'\n' ../get_second_aqms_trigger_event.sh {} > aqmsrt_trig_${DATA_FOLDER}_events_alltypes.txt

# From toplines file, get list of events that did not start as RT subnet triggers - combination of automatic RT events + PP-Jiggle events
awk '!/RT/' ${DATA_FOLDER}_toplines.txt > ${DATA_FOLDER}_toplines_from_events.txt

# Get first event origin from eventhist, for all events that did not start as RT subnet triggers, with get_first_aqms_event.sh
# This output file should not contain "hypo"
# OK for this output file to contain:
#  "A  RT": first origin from automatic RT (PickEW)
#  "C  RT": first origin from automatic RT canceled in DRP, but later finalized as event in Jiggle
#  "H  RT": first origin from automatic RT human reviewed in DRP, usually only for M3+ events
#  "Jigg": event not detected by automatic RT; manually created during post-processing in Jiggle
awk -F, '{print $1}' ${DATA_FOLDER}_toplines_from_events.txt | xargs -I{} -d'\n' ../get_first_aqms_event.sh {} > ${DATA_FOLDER}_initial_event_list.txt

# Get first event origin from eventhist, for events that started from automatic RT
awk '/RT/' ${DATA_FOLDER}_initial_event_list.txt > ${DATA_FOLDER}_initial_event_list_RT.txt

# Final RT(event) output: Get first event origins for events that started from automatic RT with get_sortedfirst_aqms_event.sh (get orid for first event) 
# This output file should not contain "hypo"; should only have "Ml" not "Me" for magnitude
awk -F, '{print $1}' ${DATA_FOLDER}_initial_event_list_RT.txt | xargs -I{} -d'\n' ../get_sortedfirst_aqms_event.sh {} "/RT/" > aqmsrt_eq_${DATA_FOLDER}_events_alltypes.txt

# Get first event origin from eventhist, for events that started from PP-jiggle
awk '/Jigg/' ${DATA_FOLDER}_initial_event_list.txt > ${DATA_FOLDER}_initial_event_list_Jigg.txt

# Final RT(event) output: Get first event origins for events that started from PP-jiggle with get_sortedfirst_aqms_event.sh (get orid for first event) 
awk -F, '{print $1}' ${DATA_FOLDER}_initial_event_list_Jigg.txt | xargs -I{} -d'\n' ../get_sortedfirst_aqms_event.sh {} "/Jigg/" > aqmspp_jiggle_${DATA_FOLDER}_events_alltypes.txt 
