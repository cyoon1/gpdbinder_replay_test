#!/bin/bash

# Convert catalog in USGS ComCat API format to a plain text catalog
#
# 2020-11-02	C. Yoon    First created

in_comcat_file='../data/comcat_20190704_73hr_events_alltypes.txt'
out_catalog_file='../data/20190704_73hr_comcat_event_catalog.txt'
catalog_start_time='2019-07-04T00:00:00'

#in_comcat_file='../data/comcat_20200810_4hr_events.txt'
#out_catalog_file='../data/20200810_4hr_comcat_event_catalog.txt'
#catalog_start_time='2020-08-10T00:00:00'

#in_comcat_file='../data/comcat_20200930_1545hr_events.txt'
#out_catalog_file='../data/20200930_1545hr_comcat_event_catalog.txt'
#catalog_start_time='2020-09-30T00:00:00'

#in_comcat_file='../data/comcat_20200930_24hr_events_alltypes.txt'
#out_catalog_file='../data/20200930_24hr_comcat_event_catalog.txt'
#catalog_start_time='2020-09-30T00:00:00'

#in_comcat_file='../data/comcat_20210104_24hr_events_alltypes.txt'
#out_catalog_file='../data/20210104_24hr_comcat_event_catalog.txt'
#catalog_start_time='2021-01-04T00:00:00'

#in_comcat_file='../data/comcat_20210605_24hr_events_alltypes.txt'
#out_catalog_file='../data/20210605_24hr_comcat_event_catalog.txt'
#catalog_start_time='2021-06-05T00:00:00'

#in_comcat_file='../data/comcat_20220325_25hr_events_alltypes.txt'
#out_catalog_file='../data/20220325_25hr_comcat_event_catalog.txt'
#catalog_start_time='2022-03-25T18:00:00'

#in_comcat_file='../data/comcat_20220408_4d_events_alltypes.txt'
#out_catalog_file='../data/20220408_4d_comcat_event_catalog.txt'
#catalog_start_time='2022-04-08T00:30:00'

#in_comcat_file='../data/comcat_20220421_8d_events_alltypes.txt'
#out_catalog_file='../data/20220421_8d_comcat_event_catalog.txt'
#catalog_start_time='2022-04-21T23:16:00'

# First column will have epoch time, then latitude, longitude, depth, magnitude, eventid
#paste <(awk -F'|' 'NR>1 {system("gdate -d"$2" +%s")}' ${in_comcat_file}) <(awk -F'|' 'NR>1 {print $3, $4, $5, $11, $1}' ${in_comcat_file}) > tmp.txt
paste <(awk -F'|' 'NR>1 {system("gdate -d"$2" +%s.%N")}' ${in_comcat_file}) <(awk -F'|' 'NR>1 {print $2, $3, $4, $5, $11, $1}' ${in_comcat_file}) > tmp.txt

# Remove catalog_start_time from epoch time
catalog_start_epoch=`gdate -d${catalog_start_time} +%s.%N`
echo ${catalog_start_epoch}

# Output plain text catalog with columns: time(sec) since catalog_start_time, latitude, longitude, depth, magnitude, eventid
#awk -v ctime=${catalog_start_epoch} '{printf "%15.5f %8.5f %8.5f %6.4f %5.3f %s\n", $1-ctime, $2, $3, $4, $5, $6}' tmp.txt > ${out_catalog_file}
awk -v ctime=${catalog_start_epoch} '{printf "%15.5f %s %8.5f %8.5f %6.4f %5.3f %s\n", $1-ctime, $2, $3, $4, $5, $6, $7}' tmp.txt > ${out_catalog_file}
rm tmp.txt

