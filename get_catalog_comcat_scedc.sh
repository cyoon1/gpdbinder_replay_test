#!/bin/bash

# Get catalog with USGS ComCat API
# https://earthquake.usgs.gov/fdsnws/event/1/
#
# 2020-11-02	C. Yoon    First created

# Catalog input/output - times
# Keep track of date when catalog was downloaded, since catalog changes over time (from analyst progress)

t_start='2019-07-04T00:00:00'
t_end='2019-07-07T01:14:00'
#out_file='../data/comcat_all_20190704_73hr_events_alltypes.txt'
out_file='../data/comcat_20190704_73hr_events_alltypes.txt'

#t_start='2020-08-10T20:15:00'
#t_end='2020-08-11T00:15:00'
#out_file='../data/comcat_20200810_4hr_events.txt'

#t_start='2020-09-30T00:00:00'
#t_end='2020-09-30T15:45:00'
##out_file='../data/comcat_all_20200930_1545hr_events.txt'
#out_file='../data/comcat_20200930_1545hr_events.txt'

#t_start='2020-09-30T00:00:00'
#t_end='2020-10-01T00:00:00'
##out_file='../data/comcat_all_20200930_24hr_events_alltypes.txt'
#out_file='../data/comcat_20200930_24hr_events_alltypes.txt'

#t_start='2021-01-04T00:00:00'
#t_end='2021-01-05T00:00:00'
##out_file='../data/comcat_all_20210104_24hr_events_alltypes.txt'
#out_file='../data/comcat_20210104_24hr_events_alltypes.txt'

#t_start='2022-03-25T18:00:00'
#t_end='2022-03-26T19:00:00'
##out_file='../data/comcat_all_20220325_25hr_events_alltypes.txt'
#out_file='../data/comcat_20220325_25hr_events_alltypes.txt'
##out_file='../data/comcat_20220325_25hr_events.txt'

#t_start='2021-06-05T00:00:00'
#t_end='2021-06-06T00:00:00'
##out_file='../data/comcat_all_20210605_24hr_events_alltypes.txt'
#out_file='../data/comcat_20210605_24hr_events_alltypes.txt'

#t_start='2022-04-08T00:30:00'
#t_end='2022-04-12T15:22:00'
##out_file='../data/comcat_all_20220408_4d_events_alltypes.txt'
#out_file='../data/comcat_20220408_4d_events_alltypes.txt'

#t_start='2022-04-21T23:16:00'
#t_end='2022-04-30T15:21:00'
##out_file='../data/comcat_all_20220421_8d_events_alltypes.txt'
#out_file='../data/comcat_20220421_8d_events_alltypes.txt'

# Catalog lat/lon boundaries
min_lat=30
max_lat=39
min_lon=-124
max_lon=-111
out_format='text'

#---------------------------------------

data_src='https://earthquake.usgs.gov/fdsnws/event/1/query'
echo ${data_src}

## Minimum magnitude query
#min_mag=5
#out_file='../Catalog/test_catalog_large.txt'
#query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&orderby='time-asc'\&minmagnitude=${min_mag}

# No minimum magnitude query (second line: specific catalog and contributor)
#query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&orderby='time-asc' # all events within region
query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&catalog=ci\&contributor=ci\&orderby='time-asc' # all ci events within region (including quarry blasts)
#query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&catalog=ci\&contributor=ci\&orderby='time-asc'\&eventtype=earthquake # all ci earthquakes within region

# Retrieve catalog from ComCat
echo ${query_str}
wget ${query_str} -O ${out_file}



