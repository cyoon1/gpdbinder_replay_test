import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

rcParams.update({'font.size': 32})
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"


in_event_mag_file = '../../reposGPD_InterpTwo/GPD_Outputs_90sFixed/GPD_compareEvents.csv'
in_plot_title = 'Magnitude 4+ earthquakes,\n 1984-2021'
out_plot_magfreq_file = '../figs/mag_freq_dist_allmag4_1984_2021.pdf'

#in_event_mag_file = '../../reposGPD_Interp/GPD_Outputs_ALL2020/GPD_compareEvents.csv'
#in_plot_title = 'Analyst-finalized earthquakes\n in 2020'
#out_plot_magfreq_file = '../figs/mag_freq_dist_all_finalized_events_2020.pdf'



[ev_id, ev_mag] = np.genfromtxt(in_event_mag_file, dtype='str,float', delimiter=',', usecols=(0,1), skip_header=1, unpack=True)
print("len(ev_mag) = ", len(ev_mag))

plt.figure(num=1, figsize=(10,8))
plt.clf()
n, bins, patches = plt.hist([ev_mag], bins=np.arange(-0.95, 8.05, 0.1), label=[str(len(ev_mag))+' events'], color=['yellow'], edgecolor='black', linewidth=0.5, stacked=True)
plt.xlim([-1, 8])
plt.yscale('log', nonpositive='clip')
#   plt.ylim([0.1, 1000])
plt.ylim([0.1, 100000])
plt.xlabel("Magnitude")
plt.ylabel("Number of events")
plt.title(in_plot_title, pad=10)
plt.legend(prop={'size':32})
plt.tight_layout()
plt.savefig(out_plot_magfreq_file)
