from obspy.geodetics.base import gps2dist_azimuth
from collections import defaultdict
import datetime
import math
import numpy as np


# Read in catalog from comcat
def get_comcat_catalog_map(in_catalog_file):
   map_catalog = defaultdict(lambda: defaultdict(int))
   nitem = 0
   with open(in_catalog_file, 'r') as fcat:
      for line in fcat:
         split_line = line.split()
#         string_day = split_line[1][0:10] #YYYY-MM-DD
         string_day = (split_line[1][0:10]).replace('/','-') #YYYY-MM-DD
         hour = split_line[1][11:13]
         minute = split_line[1][14:16]
         second = split_line[1][17:]
         nsec = 3600*int(hour) + 60*int(minute) + float(second) # number of seconds within the day (0..86400)
         evid = split_line[6]
         print(string_day, hour, minute, second, nsec)
         split_line.append(nsec)
         map_catalog[string_day][evid] = split_line
         nitem += 1
   print("Number of events read in:", nitem)
   return map_catalog


# Script to compare catalog events with quakes2aws events

#--------------------------
in_catalog_file = '../data/20190704_73hr_scedc_event_catalog.txt'
in_quakes2aws_file = '../data/20190704_73hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
catalog_start_time='2019-07-04T00:00:00'
out_dir = '../data/'
out_match_file = out_dir+'20190704_73hr_events_MATCH_scedc_gpdbinder.txt'
out_missed_file = out_dir+'20190704_73hr_events_MISSED_scedc_gpdbinder.txt'
out_new_file = out_dir+'20190704_73hr_events_NEW_scedc_gpdbinder.txt'

#in_catalog_file = '../data/20190704_73hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20190704_73hr_aqmsrt_pickew_event_catalog.txt'
#catalog_start_time='2019-07-04T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20190704_73hr_events_MATCH_scedc_pickew.txt'
#out_missed_file = out_dir+'20190704_73hr_events_MISSED_scedc_pickew.txt'
#out_new_file = out_dir+'20190704_73hr_events_NEW_scedc_pickew.txt'

#in_catalog_file = '../data/20190704_73hr_aqmsrt_pickew_event_catalog.txt'
#in_quakes2aws_file = '../data/20190704_73hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2019-07-04T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20190704_73hr_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20190704_73hr_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20190704_73hr_events_NEW_pickew_gpdbinder.txt'

#in_catalog_file = '../data/20200810_4hr_comcat_event_catalog.txt'
#in_quakes2aws_file = '../data/20200810_4hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2020-08-10T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20200810_4hr_events_MATCH_comcat_gpdbinder.txt'
#out_missed_file = out_dir+'20200810_4hr_events_MISSED_comcat.txt'
#out_new_file = out_dir+'20200810_4hr_events_NEW_gpdbinder.txt'

#in_catalog_file = '../data/20200930_1545hr_comcat_event_catalog.txt'
#in_quakes2aws_file = '../data/20200930_1545hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20200930_1545hr_events_MATCH_comcat_gpdbinder.txt'
#out_missed_file = out_dir+'20200930_1545hr_events_MISSED_comcat_gpdbinder.txt'
#out_new_file = out_dir+'20200930_1545hr_events_NEW_comcat_gpdbinder.txt'

#in_catalog_file = '../data/20200930_1545hr_comcat_event_catalog.txt'
#in_quakes2aws_file = '../data/20200930_1545hr_antarctic_rtdbn_pickew_events_v0.txt'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20200930_1545hr_events_MATCH_comcat_pickew.txt'
#out_missed_file = out_dir+'20200930_1545hr_events_MISSED_comcat_pickew.txt'
#out_new_file = out_dir+'20200930_1545hr_events_NEW_comcat_pickew.txt'

#in_catalog_file = '../data/20200930_1545hr_antarctic_rtdbn_pickew_events_v0.txt'
#in_quakes2aws_file = '../data/20200930_1545hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20200930_1545hr_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20200930_1545hr_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20200930_1545hr_events_NEW_pickew_gpdbinder.txt'

#in_catalog_file = '../data/20200930_24hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20200930_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20200930_24hr_events_MATCH_scedc_gpdbinder.txt'
#out_missed_file = out_dir+'20200930_24hr_events_MISSED_scedc_gpdbinder.txt'
#out_new_file = out_dir+'20200930_24hr_events_NEW_scedc_gpdbinder.txt'

#in_catalog_file = '../data/20200930_24hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20200930_24hr_aqmsrt_pickew_event_catalog.txt'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20200930_24hr_events_MATCH_scedc_pickew.txt'
#out_missed_file = out_dir+'20200930_24hr_events_MISSED_scedc_pickew.txt'
#out_new_file = out_dir+'20200930_24hr_events_NEW_scedc_pickew.txt'

#in_catalog_file = '../data/20200930_24hr_aqmsrt_pickew_event_catalog.txt'
#in_quakes2aws_file = '../data/20200930_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20200930_24hr_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20200930_24hr_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20200930_24hr_events_NEW_pickew_gpdbinder.txt'

#in_catalog_file = '../data/20210104_24hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20210104_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2021-01-04T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20210104_24hr_events_MATCH_scedc_gpdbinder.txt'
#out_missed_file = out_dir+'20210104_24hr_events_MISSED_scedc_gpdbinder.txt'
#out_new_file = out_dir+'20210104_24hr_events_NEW_scedc_gpdbinder.txt'

#in_catalog_file = '../data/20210104_24hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20210104_24hr_aqmsrt_pickew_event_catalog.txt'
#catalog_start_time='2021-01-04T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20210104_24hr_events_MATCH_scedc_pickew.txt'
#out_missed_file = out_dir+'20210104_24hr_events_MISSED_scedc_pickew.txt'
#out_new_file = out_dir+'20210104_24hr_events_NEW_scedc_pickew.txt'

#in_catalog_file = '../data/20210104_24hr_aqmsrt_pickew_event_catalog.txt'
#in_quakes2aws_file = '../data/20210104_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2021-01-04T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20210104_24hr_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20210104_24hr_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20210104_24hr_events_NEW_pickew_gpdbinder.txt'

#in_catalog_file = '../data/20210605_24hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20210605_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2021-06-05T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20210605_24hr_events_MATCH_scedc_gpdbinder.txt'
#out_missed_file = out_dir+'20210605_24hr_events_MISSED_scedc_gpdbinder.txt'
#out_new_file = out_dir+'20210605_24hr_events_NEW_scedc_gpdbinder.txt'

#in_catalog_file = '../data/20210605_24hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20210605_24hr_aqmsrt_pickew_event_catalog.txt'
#catalog_start_time='2021-06-05T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20210605_24hr_events_MATCH_scedc_pickew.txt'
#out_missed_file = out_dir+'20210605_24hr_events_MISSED_scedc_pickew.txt'
#out_new_file = out_dir+'20210605_24hr_events_NEW_scedc_pickew.txt'

#in_catalog_file = '../data/20210605_24hr_aqmsrt_pickew_event_catalog.txt'
#in_quakes2aws_file = '../data/20210605_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2021-06-05T00:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20210605_24hr_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20210605_24hr_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20210605_24hr_events_NEW_pickew_gpdbinder.txt'

#in_catalog_file = '../data/20220325_25hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2022-03-25T18:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220325_25hr_events_MATCH_scedc_gpdbinder.txt'
#out_missed_file = out_dir+'20220325_25hr_events_MISSED_scedc_gpdbinder.txt'
#out_new_file = out_dir+'20220325_25hr_events_NEW_scedc_gpdbinder.txt'

#in_catalog_file = '../data/20220325_25hr_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20220325_25hr_aqmsrt_pickew_event_catalog.txt'
#catalog_start_time = '2022-03-25T18:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220325_25hr_events_MATCH_scedc_pickew.txt'
#out_missed_file = out_dir+'20220325_25hr_events_MISSED_scedc_pickew.txt'
#out_new_file = out_dir+'20220325_25hr_events_NEW_scedc_pickew.txt'

#in_catalog_file = '../data/20220325_25hr_aqmsrt_pickew_event_catalog.txt'
#in_quakes2aws_file = '../data/20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2022-03-25T18:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220325_25hr_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20220325_25hr_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20220325_25hr_events_NEW_pickew_gpdbinder.txt'

#in_catalog_file = '../data/20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#in_quakes2aws_file = '../data/20220325R_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time = '2022-03-25T18:00:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220325R_25hr_events_MATCH_replay_gpdbinder.txt'
#out_missed_file = out_dir+'20220325R_25hr_events_MISSED_replay_gpdbinder.txt'
#out_new_file = out_dir+'20220325R_25hr_events_NEW_replay_gpdbinder.txt'

#in_catalog_file = '../data/20220408_4d_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20220408_4d_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2022-04-08T00:30:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220408_4d_events_MATCH_scedc_gpdbinder.txt'
#out_missed_file = out_dir+'20220408_4d_events_MISSED_scedc_gpdbinder.txt'
#out_new_file = out_dir+'20220408_4d_events_NEW_scedc_gpdbinder.txt'

#in_catalog_file = '../data/20220408_4d_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20220408_4d_aqmsrt_pickew_event_catalog.txt'
#catalog_start_time='2022-04-08T00:30:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220408_4d_events_MATCH_scedc_pickew.txt'
#out_missed_file = out_dir+'20220408_4d_events_MISSED_scedc_pickew.txt'
#out_new_file = out_dir+'20220408_4d_events_NEW_scedc_pickew.txt'

#in_catalog_file = '../data/20220408_4d_aqmsrt_pickew_event_catalog.txt'
#in_quakes2aws_file = '../data/20220408_4d_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2022-04-08T00:30:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220408_4d_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20220408_4d_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20220408_4d_events_NEW_pickew_gpdbinder.txt'

#in_catalog_file = '../data/20220421_8d_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20220421_8d_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2022-04-21T23:16:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220421_8d_events_MATCH_scedc_gpdbinder.txt'
#out_missed_file = out_dir+'20220421_8d_events_MISSED_scedc_gpdbinder.txt'
#out_new_file = out_dir+'20220421_8d_events_NEW_scedc_gpdbinder.txt'

#in_catalog_file = '../data/20220421_8d_scedc_event_catalog.txt'
#in_quakes2aws_file = '../data/20220421_8d_aqmsrt_pickew_event_catalog.txt'
#catalog_start_time='2022-04-21T23:16:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220421_8d_events_MATCH_scedc_pickew.txt'
#out_missed_file = out_dir+'20220421_8d_events_MISSED_scedc_pickew.txt'
#out_new_file = out_dir+'20220421_8d_events_NEW_scedc_pickew.txt'

#in_catalog_file = '../data/20220421_8d_aqmsrt_pickew_event_catalog.txt'
#in_quakes2aws_file = '../data/20220421_8d_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2022-04-21T23:16:00'
#out_dir = '../data/'
#out_match_file = out_dir+'20220421_8d_events_MATCH_pickew_gpdbinder.txt'
#out_missed_file = out_dir+'20220421_8d_events_MISSED_pickew_gpdbinder.txt'
#out_new_file = out_dir+'20220421_8d_events_NEW_pickew_gpdbinder.txt'

#--------------------------


# Get map (dictionary) with catalog event info
# Key is YYYYMMDD for faster search
map_catalog = get_comcat_catalog_map(in_catalog_file)

# Criteria for matching a comcat catalog event with quakes2aws event
#delta_match_sec = 3.0 # seconds
delta_match_sec = 5.0 # seconds
#delta_distance_thresh = 10.0 # km
delta_distance_thresh = 25.0 # km

# Loop over Quakes2AWS events in file
diff_cat_arr = []
fmatch_out = open(out_match_file, 'w')
num_match = 0
fnew_out = open(out_new_file, 'w')
num_new = 0
with open(in_quakes2aws_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
#      origin_time = datetime.datetime.strptime(split_line[1], "%Y/%m/%dT%H:%M:%S.%f")
      origin_time = datetime.datetime.strptime(split_line[1], "%Y-%m-%dT%H:%M:%S.%f")
      origin_time_second = round(origin_time.second + 1e-6*origin_time.microsecond)
      cur_nsec = 3600*origin_time.hour + 60*origin_time.minute + origin_time_second
      string_day = datetime.datetime.strftime(origin_time, "%Y-%m-%d")
      print(origin_time, cur_nsec, string_day)

      flag_in_catalog = False
      if (string_day in map_catalog):
         cat_day = map_catalog[string_day] # Contains all catalog events with same YYYYMMDD as eqt event

         tmp_dict = defaultdict(int)
         for icat in cat_day:
            diff_cat_det = cur_nsec - cat_day[icat][-1]
            if (abs(diff_cat_det) <= delta_match_sec):
               tmp_dict[icat] = abs(diff_cat_det)

         # Now tmp_dict has all catalog events with origin time within delta_match_sec of eqt event
         if (len(tmp_dict) > 0):
            for imatch in sorted(tmp_dict, key=tmp_dict.get): # traverse in time order
               item_sec = imatch
               print(imatch, map_catalog[string_day][imatch])
               print(line.strip('\n'))
               print("imatch = ", imatch, ", time difference: ", tmp_dict[imatch])

               cat_lat = float(map_catalog[string_day][item_sec][2])
               cat_lon = float(map_catalog[string_day][item_sec][3])
               cat_depth = float(map_catalog[string_day][item_sec][4])
               cat_mag = float(map_catalog[string_day][item_sec][5])
               print("catalog values: ", cat_lat, cat_lon, cat_depth, cat_mag)

               q2a_lat = float(split_line[2])
               q2a_lon = float(split_line[3])
               q2a_depth = float(split_line[4])
               print("quakes2aws lat,lon,depth: ", q2a_lat, q2a_lon, q2a_depth)

               # Check epicentral distance between catalog event and q2a event
               [epi_dist, azAB, azBA] = gps2dist_azimuth(cat_lat, cat_lon, q2a_lat, q2a_lon)
               epi_dist_km = 0.001*epi_dist
               diff_depth_km = cat_depth - q2a_depth
               print("epi_dist_km = ", epi_dist_km, ", diff_depth_km = ", diff_depth_km, "\n")
               
               # If epicentral distance exceeds threshold, do not declare a match
               if (epi_dist_km > delta_distance_thresh):
                  print("WARNING: epi_dist_km above threshold: ", delta_distance_thresh, "\n")
                  continue

               # Found a match between catalog event and q2a event
               flag_in_catalog = True
               num_match += 1
               fmatch_out.write(('%s %s %7.4f %7.4f %5.3f %4.2f %s\n') % (line.strip('\n'), map_catalog[string_day][item_sec][1], cat_lat, cat_lon, cat_depth, cat_mag, map_catalog[string_day][item_sec][6].strip('ci')))
               diff_cat_arr.append(cur_nsec-tmp_dict[imatch])
               map_catalog[string_day].pop(item_sec) # Remove matching items from catalog map

               if (flag_in_catalog): # only want one matching item with lowest abs(diff_cat_det)
                  break

      # Did not find a match with catalog event; q2a event must be new
      if (not flag_in_catalog): # Detection not found in catalog
         num_new += 1
         fnew_out.write(('%s') % line)

fmatch_out.close()
fnew_out.close()

diff_cat_arr = np.asarray(diff_cat_arr)
print("len(diff_cat_arr) = ", len(diff_cat_arr))
print("min(diff_cat_arr) = ", min(diff_cat_arr))
print("max(diff_cat_arr) = ", max(diff_cat_arr))
print("max(abs(diff_cat_arr)) = ", max(abs(diff_cat_arr)))
#np.savetxt('diff_'+network_str+'.txt', np.sort(diff_cat_arr), fmt='%d')

# Only missed events should remain in catalog map, so write them out
catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
num_missed = 0
fmissed_out = open(out_missed_file, 'w')
for iday in map_catalog:
   for event in map_catalog[iday]:
      num_missed += 1
      # Write: time_diff, origin_time, lat, lon, depth, mag, evid
#      origin_time = datetime.datetime.strptime(map_catalog[iday][event][1], "%Y-%m-%dT%H:%M:%S.%f")
      origin_time = datetime.datetime.strptime(map_catalog[iday][event][1], "%Y/%m/%dT%H:%M:%S.%f")
      time_diff = (origin_time - catalog_ref_time).total_seconds()
      fmissed_out.write(('%15.5f %s %7.4f %7.4f %5.3f %4.2f %s\n') % (time_diff, map_catalog[iday][event][1], float(map_catalog[iday][event][2]), float(map_catalog[iday][event][3]), float(map_catalog[iday][event][4]), float(map_catalog[iday][event][5]), map_catalog[iday][event][6].strip('ci')))
fmissed_out.close()

print("Number of match catalog events: ", num_match)
print("Number of new events not in catalog: ", num_new)
print("Number of missed catalog events: ", num_missed)

