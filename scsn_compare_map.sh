#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

gmt gmtset LABEL_FONT_SIZE 20p
gmt gmtset FONT_ANNOT 20p
#gmt gmtset LABEL_FONT_SIZE 32p #poster
#gmt gmtset FONT_ANNOT 32p #poster

out_color_filename=depthcolors.cpt
out_color_file=../figs/${out_color_filename}
out_color_time_filename=timecolors.cpt
out_color_time_file=../figs/${out_color_time_filename}

#in_fault_file=../data/socalfaults.llz.txt
in_fault_file=../data/calif_flts.car

## New catalog from jiggle
#in_cat_file=../data/20210605_antarctic_rtdbn_quakes2aws_events_v0.txt
##out_map_cat_depth_file=../figs/catalog_20210605_antarctic_rtdbn_quakes2aws_events_v0_depth
#out_map_cat_depth_file=../figs/catalog_20210605_antarctic_rtdbn_quakes2aws_events_v0_zoom_depth
#out_map_cat_time_file=../figs/catalog_20210605_antarctic_rtdbn_quakes2aws_events_v0_time

## SCEDC catalog
#in_cat_file=../data/20210605_scedc_event_catalog.txt
#out_map_cat_depth_file=../figs/catalog_20210605_scedc_depth
##out_map_cat_depth_file=../figs/catalog_20210605_scedc_zoom_depth
#out_map_cat_time_file=../figs/catalog_20210605_scedc_time

#--------------------

## New catalog from jiggle
#in_cat_file=../data/20190704_73hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20190704_73hr_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20190704_73hr_aqmsrt_pickew_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_aqmsrt_pickew_depth
#flag_arrow=0

## MISSED events from pickew
#in_cat_file=../data/20190704_73hr_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20190704_73hr_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20190704_73hr_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20190704_73hr_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1

# SCEDC catalog
in_cat_file=../data/20190704_73hr_scedc_event_catalog.txt
out_map_cat_depth_base=../figs/catalog_20190704_73hr_scedc_depth
#out_map_cat_time_file=../figs/catalog_20190704_73hr_scedc_time
flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20190704_73hr_events_MISSED_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_MISSED_scedc_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20190704_73hr_events_NEW_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_NEW_scedc_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20190704_73hr_events_MATCH_scedc_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_MATCH_scedc_gpdbinder_f_scedc_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20190704_73hr_events_MATCH_scedc_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20190704_73hr_events_MATCH_scedc_gpdbinder_f_scedc_depth
#flag_arrow=1

#--------------------
## New catalog from jiggle
#in_cat_file=../data/20200810_4hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20200810_4hr_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20200810_4hr_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## SCEDC catalog
#in_cat_file=../data/20200810_4hr_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20200810_4hr_scedc_depth
##out_map_cat_time_file=../figs/catalog_20200810_4hr_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20200810_4hr_events_MISSED_comcat.txt
#out_map_cat_depth_base=../figs/catalog_20200810_4hr_events_MISSED_comcat_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20200810_4hr_events_NEW_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200810_4hr_events_NEW_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20200810_4hr_events_MATCH_comcat_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20200810_4hr_events_MATCH_comcat_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20200810_4hr_events_MATCH_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20200810_4hr_events_MATCH_comcat_depth
#flag_arrow=1
#--------------------
## New catalog from jiggle
#in_cat_file=../data/20200930_1545hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20200930_1545hr_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20200930_1545hr_antarctic_rtdbn_pickew_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_antarctic_rtdbn_pickew_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20200930_1545hr_antarctic_rtdbn_pickew_events_v0_time
#flag_arrow=0

## SCEDC catalog
#in_cat_file=../data/20200930_1545hr_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_scedc_depth
##out_map_cat_time_file=../figs/catalog_20200930_1545hr_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20200930_1545hr_events_MISSED_comcat_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MISSED_comcat_gpdbinder_depth
#flag_arrow=0
#
## NEW events from gpdbinder
#in_cat_file=../data/20200930_1545hr_events_NEW_comcat_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_NEW_comcat_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20200930_1545hr_events_MATCH_comcat_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MATCH_comcat_gpdbinder_f_comcat_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MATCH_comcat_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20200930_1545hr_events_MATCH_comcat_gpdbinder_f_comcat_depth
#flag_arrow=1

## MISSED events (by pickew) from catalog
#in_cat_file=../data/20200930_1545hr_events_MISSED_comcat_pickew.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MISSED_comcat_pickew_depth
#flag_arrow=0
#
## MATCH events from catalog and pickew
#in_cat_file=../data/20200930_1545hr_events_MATCH_comcat_pickew.txt
##out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MATCH_comcat_pickew_f_comcat_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MATCH_comcat_pickew_f_pickew_depth
#out_map_cat_depth2_base=../figs/catalog_20200930_1545hr_events_MATCH_comcat_pickew_f_comcat_depth
#flag_arrow=1

## MISSED events from pickew
#in_cat_file=../data/20200930_1545hr_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0
#
## NEW events from gpdbinder
#in_cat_file=../data/20200930_1545hr_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20200930_1545hr_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20200930_1545hr_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20200930_1545hr_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1
#--------------------

## New catalog from jiggle
#in_cat_file=../data/20200930_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20200930_24hr_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20200930_24hr_aqmsrt_pickew_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_aqmsrt_pickew_depth
#flag_arrow=0

## MISSED events from pickew
#in_cat_file=../data/20200930_24hr_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20200930_24hr_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20200930_24hr_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20200930_24hr_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1

## SCEDC catalog
#in_cat_file=../data/20200930_24hr_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_scedc_depth
##out_map_cat_time_file=../figs/catalog_20200930_24hr_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20200930_24hr_events_MISSED_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_MISSED_scedc_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20200930_24hr_events_NEW_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_NEW_scedc_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20200930_24hr_events_MATCH_scedc_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_MATCH_scedc_gpdbinder_f_scedc_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20200930_24hr_events_MATCH_scedc_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20200930_24hr_events_MATCH_scedc_gpdbinder_f_scedc_depth
#flag_arrow=1

#--------------------

## New catalog from jiggle
#in_cat_file=../data/20210104_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20210104_24hr_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20210104_24hr_aqmsrt_pickew_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_aqmsrt_pickew_depth
#flag_arrow=0

## MISSED events from pickew
#in_cat_file=../data/20210104_24hr_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20210104_24hr_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20210104_24hr_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20210104_24hr_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1

## SCEDC catalog
#in_cat_file=../data/20210104_24hr_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_scedc_depth
##out_map_cat_time_file=../figs/catalog_20210104_24hr_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20210104_24hr_events_MISSED_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_MISSED_scedc_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20210104_24hr_events_NEW_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_NEW_scedc_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20210104_24hr_events_MATCH_scedc_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_MATCH_scedc_gpdbinder_f_scedc_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20210104_24hr_events_MATCH_scedc_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20210104_24hr_events_MATCH_scedc_gpdbinder_f_scedc_depth
#flag_arrow=1

#--------------------

## New catalog from jiggle
#in_cat_file=../data/20210605_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20210605_24hr_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20210605_24hr_aqmsrt_pickew_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_aqmsrt_pickew_depth
#flag_arrow=0

## MISSED events from pickew
#in_cat_file=../data/20210605_24hr_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20210605_24hr_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20210605_24hr_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20210605_24hr_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1

## SCEDC catalog
#in_cat_file=../data/20210605_24hr_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_scedc_depth
##out_map_cat_time_file=../figs/catalog_20210605_24hr_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20210605_24hr_events_MISSED_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_MISSED_scedc_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20210605_24hr_events_NEW_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_NEW_scedc_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20210605_24hr_events_MATCH_scedc_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_MATCH_scedc_gpdbinder_f_scedc_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20210605_24hr_events_MATCH_scedc_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20210605_24hr_events_MATCH_scedc_gpdbinder_f_scedc_depth
#flag_arrow=1

#--------------------

## New catalog from jiggle
#in_cat_file=../data/20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20220325_25hr_aqmsrt_pickew_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_aqmsrt_pickew_depth
#flag_arrow=0

## MISSED events from pickew
#in_cat_file=../data/20220325_25hr_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20220325_25hr_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20220325_25hr_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20220325_25hr_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1

## SCEDC catalog
#in_cat_file=../data/20220325_25hr_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_scedc_depth
##out_map_cat_time_file=../figs/catalog_20220325_25hr_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20220325_25hr_events_MISSED_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_MISSED_scedc_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20220325_25hr_events_NEW_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_NEW_scedc_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20220325_25hr_events_MATCH_scedc_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_MATCH_scedc_gpdbinder_f_scedc_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20220325_25hr_events_MATCH_scedc_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20220325_25hr_events_MATCH_scedc_gpdbinder_f_scedc_depth
#flag_arrow=1

## New catalog from jiggle
#in_cat_file=../data/20220325R_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20220325R_25hr_antarctic_rtdbn_quakes2aws_events_v0_depth
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20220325R_25hr_events_MISSED_replay_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220325R_25hr_events_MISSED_replay_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20220325R_25hr_events_NEW_replay_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220325R_25hr_events_NEW_replay_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20220325R_25hr_events_MATCH_replay_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20220325R_25hr_events_MATCH_replay_gpdbinder_f_realtime_depth # need to comment out ### lines
#flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20220325R_25hr_events_MATCH_replay_gpdbinder_f_replay_depth
##out_map_cat_depth2_base=../figs/catalog_20220325R_25hr_events_MATCH_replay_gpdbinder_f_replay_depth
##flag_arrow=1

#--------------------

## New catalog from jiggle
#in_cat_file=../data/20220408_4d_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20220408_4d_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20220408_4d_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20220408_4d_aqmsrt_pickew_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20220408_4d_aqmsrt_pickew_depth
#flag_arrow=0

## MISSED events from pickew
#in_cat_file=../data/20220408_4d_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220408_4d_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20220408_4d_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220408_4d_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20220408_4d_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20220408_4d_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20220408_4d_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20220408_4d_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1

## SCEDC catalog
#in_cat_file=../data/20220408_4d_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20220408_4d_scedc_depth
##out_map_cat_time_file=../figs/catalog_20220408_4d_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20220408_4d_events_MISSED_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220408_4d_events_MISSED_scedc_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20220408_4d_events_NEW_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220408_4d_events_NEW_scedc_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20220408_4d_events_MATCH_scedc_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20220408_4d_events_MATCH_scedc_gpdbinder_f_scedc_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20220408_4d_events_MATCH_scedc_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20220408_4d_events_MATCH_scedc_gpdbinder_f_scedc_depth
#flag_arrow=1

#--------------------

## New catalog from jiggle
#in_cat_file=../data/20220421_8d_antarctic_rtdbn_quakes2aws_events_v0.txt
#out_map_cat_depth_base=../figs/catalog_20220421_8d_antarctic_rtdbn_quakes2aws_events_v0_depth
##out_map_cat_time_file=../figs/catalog_20220421_8d_antarctic_rtdbn_quakes2aws_events_v0_time
#flag_arrow=0

## Automatic catalog from pickew
#in_cat_file=../data/20220421_8d_aqmsrt_pickew_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20220421_8d_aqmsrt_pickew_depth
#flag_arrow=0

## MISSED events from pickew
#in_cat_file=../data/20220421_8d_events_MISSED_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220421_8d_events_MISSED_pickew_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20220421_8d_events_NEW_pickew_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220421_8d_events_NEW_pickew_gpdbinder_depth
#flag_arrow=0

## MATCH events from pickew and gpdbinder
#in_cat_file=../data/20220421_8d_events_MATCH_pickew_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20220421_8d_events_MATCH_pickew_gpdbinder_f_pickew_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20220421_8d_events_MATCH_pickew_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20220421_8d_events_MATCH_pickew_gpdbinder_f_pickew_depth
#flag_arrow=1

## SCEDC catalog
#in_cat_file=../data/20220421_8d_scedc_event_catalog.txt
#out_map_cat_depth_base=../figs/catalog_20220421_8d_scedc_depth
##out_map_cat_time_file=../figs/catalog_20220421_8d_scedc_time
#flag_arrow=0

## MISSED events from catalog
#in_cat_file=../data/20220421_8d_events_MISSED_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220421_8d_events_MISSED_scedc_gpdbinder_depth
#flag_arrow=0

## NEW events from gpdbinder
#in_cat_file=../data/20220421_8d_events_NEW_scedc_gpdbinder.txt
#out_map_cat_depth_base=../figs/catalog_20220421_8d_events_NEW_scedc_gpdbinder_depth
#flag_arrow=0

## MATCH events from catalog and gpdbinder
#in_cat_file=../data/20220421_8d_events_MATCH_scedc_gpdbinder.txt
##out_map_cat_depth_base=../figs/catalog_20220421_8d_events_MATCH_scedc_gpdbinder_f_scedc_depth # need to comment out ### lines
##flag_arrow=0
#out_map_cat_depth_base=../figs/catalog_20220421_8d_events_MATCH_scedc_gpdbinder_f_gpdbinder_depth
#out_map_cat_depth2_base=../figs/catalog_20220421_8d_events_MATCH_scedc_gpdbinder_f_scedc_depth
#flag_arrow=1

#--------------------

# All California
#min_lat=32
#max_lat=37
#min_lon=-120
#max_lon=-114
#scale_lat=32.4
#scale_lon=-115.5
min_lat=31
max_lat=38
min_lon=-121.5
max_lon=-113.5
scale_lat=32.4
scale_lon=-118.4
scale_dist_km=200
proj=-JM15
lat_lon_spacing=1
colorbar_x=6.0
flag_proj=0
out_map_cat_depth_file=${out_map_cat_depth_base}_zoomA

## Zoom in Salton Sea
#min_lat=32.5
#max_lat=34
#min_lon=-117
#max_lon=-115
#scale_lat=32.7
#scale_lon=-116
#scale_dist_km=100
#proj=-JM16
#lat_lon_spacing=0.5
#colorbar_x=6.4
#flag_proj=0
#out_map_cat_depth_file=${out_map_cat_depth_base}_zoomB

## Zoom in Ridgecrest
#min_lat=35.3
#max_lat=36.2
#min_lon=-118.0
#max_lon=-117.2
#scale_lat=35.35
#scale_lon=-117.6
#scale_dist_km=20
#proj=-JM17
#lat_lon_spacing=0.2
#colorbar_x=6.8
#flag_proj=0
#out_map_cat_depth_file=${out_map_cat_depth_base}_zoomB

## Zoom in detail - 20200810_4hr
#min_lat=33.0
#max_lat=33.4
#min_lon=-115.8
#max_lon=-115.4
#scale_lat=33.05
#scale_lon=-115.6
#scale_dist_km=20
#proj=-JM17
#lat_lon_spacing=0.1
#colorbar_x=6.8
#flag_proj=1
#out_map_cat_depth_file=${out_map_cat_depth_base}_zoomC
#out_map_cat_depth2_file=${out_map_cat_depth2_base}_zoomC

## Zoom in detail - 20200930_1545hr
#min_lat=32.85
#max_lat=33.25
#min_lon=-115.8
#max_lon=-115.4
#scale_lat=32.9
#scale_lon=-115.6
#scale_dist_km=20
#proj=-JM17
#lat_lon_spacing=0.1
#colorbar_x=6.8
#flag_proj=1
#out_map_cat_depth_file=${out_map_cat_depth_base}_zoomC
#out_map_cat_depth2_file=${out_map_cat_depth2_base}_zoomC
#
##
##
###region_inset=-R-75/-62/15/22
###projection_inset=-JM2.0i
##
### Cross section parameters - 20200810_4hr
##strike1_angle=90
##strike1_length=15
##strike1_proj_width=15
##strike1_center_lat=33.25
##strike1_center_lon=-115.65
##strike1_bproj=-JX15/10
##strike1_brange=-R-15/15/-20/0
##strike2_angle=0
##strike2_length=15
##strike2_proj_width=15
##strike2_center_lat=33.25
##strike2_center_lon=-115.65
##strike2_bproj=-JX15/10
##strike2_brange=-R-15/15/-20/0
#
## Cross section parameters - 20200930_1545hr
#strike1_angle=90
#strike1_length=15
#strike1_proj_width=15
#strike1_center_lat=33.05
#strike1_center_lon=-115.6
#strike1_bproj=-JX15/10
#strike1_brange=-R-15/15/-20/0
#strike2_angle=0
#strike2_length=15
#strike2_proj_width=15
#strike2_center_lat=33.05
#strike2_center_lon=-115.6
#strike2_bproj=-JX15/10
#strike2_brange=-R-15/15/-20/0

# Time slice ranges
FIRST_DAY=0 # 2020-08-10
LAST_DAY=12 # 2020-08-12
#

#------------END OF INPUTS-------------

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
echo ${reg}

##### COLOR BY DEPTH #####

gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}

#gmt begin ${out_map_cat_depth_file}
gmt begin ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G230 -Slightskyblue -Lg${scale_lon}/${scale_lat}+c${scale_lon}/${scale_lat}+w${scale_dist_km}+f+lkm

#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $4, $5, $6*0.3*0.3}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_file}
###   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $9, $10, $11, $12*0.3*0.3}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_file}
   # Nice plots for talk
#   gmt makecpt -Celevation -I > grayout.cpt
#   gmt grdimage @earth_relief_02m -I+a15+ne0.75 -Cgrayout.cpt
#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $4, $5, $6*0.3*0.3}' | gmt plot ${proj} ${reg} -Sc -W0.5 -BneWS -: -C${out_color_file}
   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $4, $5, $6*0.3*0.3}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_file}

   gmt plot ${proj} ${reg} -W0.01p ${in_fault_file}
   gmt colorbar -C${out_color_file} -Dx${colorbar_x}i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # SCSN region polygon
   gmt plot ${proj} ${reg} -W1,black << EOF
-117.76 37.43
-121.25 34.5
-118.5 31.5
-114 31.5
-114 34.5
-117.76 37.43
EOF

   if [ ${flag_proj} -eq 1 ]
   then
      if [ ${flag_arrow} -eq 1 ]
      then
	 # Draw arrows from gpdbinder to comcat locations
	 sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $4, $5, $9, $10}' | gmt plot ${proj} ${reg} -Sv0.2+e+s -W0.5+c -BneWS -: -C${out_color_file}
      fi

      # Project seismicity along one direction
     awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $4, $3, $5, $7, $6, $1}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt # plot all events
     awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $4, $3, $5, $7, $6, $1}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt # plot all events
###     awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $10, $9, $11, $13, $12, $1}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt # plot all events
###     awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $10, $9, $11, $13, $12, $1}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt # plot all events

      # Projection lines
      gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W1,0/0/0,- -Gblack
#      gmt text ${proj} ${reg} -F+f24 << EOF
#-115.79 33.24 A
#-115.5 33.24 A'
#EOF
      gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W1,0/0/0,- -Gblack
#      gmt text ${proj} ${reg} -F+f24 << EOF
#-115.64 33.11 B
#-115.64 33.38 B'
#EOF
   fi # if [ ${flag_proj} -eq 1 ]

#   # Add inset
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
#   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
#   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}
#EOF
gmt end show



##### COLOR BY TIME #####

#gmt makecpt -Cviridis -I -T0/2/0.01 > ${out_color_time_file} # 2020-08-10 4hr
#start_date=2020-08-10
#
##gmt begin ${out_map_cat_time_file}
#gmt begin ${out_map_cat_time_file}_${FIRST_DAY}_${LAST_DAY}
#   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
#   gmt coast ${proj} ${reg} -W0.25p,black -Na -G210 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
##   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   #awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
##   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172}' ${in_cat_file} | gmt plot ${proj} ${reg} -Scc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
##   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Scc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.3+cl -BneWS -: -C${out_color_time_file}
#   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
#   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
##   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
#
#   # Projection lines
#   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-66.82 18.15 A'
#-66.95 17.65 A
#EOF
#   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-67.25 17.95 B
#-66.45 17.90 B'
#EOF
#
##   # Add inset
##   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
##   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
##   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
##${min_lon} ${min_lat}
##${min_lon} ${max_lat}
##${max_lon} ${max_lat}
##${max_lon} ${min_lat}
##${min_lon} ${min_lat}
##EOF
#gmt end show
#

if [ ${flag_proj} -eq 1 ]
then

   gmt gmtset LABEL_FONT_SIZE 32p
   gmt gmtset FONT_ANNOT 32p
   #gmt gmtset LABEL_FONT_SIZE 56p #poster
   #gmt gmtset FONT_ANNOT 56p #poster

   #gmt begin ${out_map_cat_time_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}
   gmt begin ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}
      gmt basemap ${strike1_bproj} ${strike1_brange} -Ba10f5 -BneWS+g230 #-U
      sort -nk6,6 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $3, $5*0.1}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa5+l"Length along cross-section A-A' (km)" -Bya5+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_file}

      if [ ${flag_arrow} -eq 1 ]
      then
	 # Draw arrows from gpdbinder to comcat locations
	 paste <(sort -nk6,6 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $3, $5*0.1}') <(sort -nk6,6 ${out_map_cat_depth2_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $3, $5*0.1}') > tmp.txt 
	 awk '{print $1, $2, $3, $5, $6}' tmp.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa5+l"Length along cross-section A-A' (km)" -Bya5+l"Depth (km)" -BneWS -Sv0.3+e+s -W0.5+c -C${out_color_file}
      fi

   #   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
   gmt end show

   #gmt begin ${out_map_cat_time_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}
   gmt begin ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}
      gmt basemap ${strike2_bproj} ${strike2_brange} -Ba10f5 -BneWS+g230 #-U
      sort -nk6,6 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $3, $5*0.1}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa5+l"Length along cross-section B-B' (km)" -Bya5+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_file}

      if [ ${flag_arrow} -eq 1 ]
      then
	 # Draw arrows from gpdbinder to comcat locations
	 paste <(sort -nk6,6 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $3, $5*0.1}') <(sort -nk6,6 ${out_map_cat_depth2_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $3, $5*0.1}') > tmp.txt 
	 awk '{print $1, $2, $3, $5, $6}' tmp.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa5+l"Length along cross-section B-B' (km)" -Bya5+l"Depth (km)" -BneWS -Sv0.3+e+s -W0.5+c -C${out_color_file}
      fi

   #   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show
fi
