#!/bin/bash

# Script to get first AQMS automatic event
# Runs on amosite

# get_first_aqms_event.sh
if [[ $# -eq 0 ]] ; then
   echo 'Usage: ./get_first_aqms_event.sh <eventid>'
   exit 1
fi

# Get first aqms event (real-time, binder, original event solution)
#eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | sort -t',' -nrk1,3 | awk '{$1=""; print $0}' | tail -n 1
eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | tail -n 1
