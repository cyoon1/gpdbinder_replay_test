#!/bin/bash

# Convert catalog in USGS ComCat API format to a plain text catalog
#
# 2020-11-02	C. Yoon    First created

#in_jiggle_file='../data/jiggle_20210605_antarctic_rtdbn_quakes2aws_events_v0'
#out_catalog_file='../data/20210605_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2021-06-05T00:00:00'

in_jiggle_file='../data/jiggle_20190704_73hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
out_catalog_file='../data/20190704_73hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#in_jiggle_file='../data/jiggle_20190704_73hr_antarctic_rtdbn_pickew_events_v0.txt'
#out_catalog_file='../data/20190704_73hr_antarctic_rtdbn_pickew_events_v0.txt'
catalog_start_time='2019-07-04T00:00:00'

#in_jiggle_file='../data/jiggle_20200810_4hr_antarctic_rtdbn_quakes2aws_events_v0'
#out_catalog_file='../data/20200810_4hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2020-08-10T00:00:00'

##in_jiggle_file='../data/jiggle_20200930_1545hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
##out_catalog_file='../data/20200930_1545hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#in_jiggle_file='../data/jiggle_20200930_1545hr_antarctic_rtdbn_pickew_events_v0.txt'
#out_catalog_file='../data/20200930_1545hr_antarctic_rtdbn_pickew_events_v0.txt'
#catalog_start_time='2020-09-30T00:00:00'

#in_jiggle_file='../data/jiggle_20200930_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#out_catalog_file='../data/20200930_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
##in_jiggle_file='../data/jiggle_20200930_24hr_antarctic_rtdbn_pickew_events_v0.txt'
##out_catalog_file='../data/20200930_24hr_antarctic_rtdbn_pickew_events_v0.txt'
#catalog_start_time='2020-09-30T00:00:00'

#in_jiggle_file='../data/jiggle_20210104_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#out_catalog_file='../data/20210104_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
##in_jiggle_file='../data/jiggle_20210104_24hr_antarctic_rtdbn_pickew_events_v0.txt'
##out_catalog_file='../data/20210104_24hr_antarctic_rtdbn_pickew_events_v0.txt'
#catalog_start_time='2021-01-04T00:00:00'

#in_jiggle_file='../data/jiggle_20210605_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#out_catalog_file='../data/20210605_24hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
##in_jiggle_file='../data/jiggle_20210605_24hr_antarctic_rtdbn_pickew_events_v0.txt'
##out_catalog_file='../data/20210605_24hr_antarctic_rtdbn_pickew_events_v0.txt'
#catalog_start_time='2021-06-05T00:00:00'

#in_jiggle_file='../data/jiggle_20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#out_catalog_file='../data/20220325_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
##in_jiggle_file='../data/jiggle_20220325R_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
##out_catalog_file='../data/20220325R_25hr_antarctic_rtdbn_quakes2aws_events_v0.txt'
#catalog_start_time='2022-03-25T18:00:00'

#in_jiggle_file='../data/jiggle_20220408_4d_antarctic_rtdbn_quakes2aws_events_v0.txt'
#out_catalog_file='../data/20220408_4d_antarctic_rtdbn_quakes2aws_events_v0.txt'
##in_jiggle_file='../data/jiggle_20220408_4d_antarctic_rtdbn_pickew_events_v0.txt'
##out_catalog_file='../data/20220408_4d_antarctic_rtdbn_pickew_events_v0.txt'
#catalog_start_time='2022-04-08T00:30:00'

#in_jiggle_file='../data/jiggle_20220421_8d_antarctic_rtdbn_quakes2aws_events_v0.txt'
#out_catalog_file='../data/20220421_8d_antarctic_rtdbn_quakes2aws_events_v0.txt'
##in_jiggle_file='../data/jiggle_20220421_8d_antarctic_rtdbn_pickew_events_v0.txt'
##out_catalog_file='../data/20220421_8d_antarctic_rtdbn_pickew_events_v0.txt'
#catalog_start_time='2022-04-21T23:16:00'

tmp_file=tmp.txt
tmp2_file=tmp2.txt
#awk -F',' 'FNR>1 {if ($12 ~ /earthquake/) printf("%sT%s|%f|%f|%f|%f|%f|%f|%f|%f|%d %d\n"),$3, $4, $5, $6, $8, $16, $17, $18, $19, $20, $22, $1}' ${in_jiggle_file} > ${tmp_file}
#awk -F',' 'FNR>1 {printf("%sT%s|%f|%f|%f|%f|%f|%f|%f|%f|%d %d\n"),$3, $4, $5, $6, $8, $16, $17, $18, $19, $20, $22, $1}' ${in_jiggle_file} > ${tmp_file}
###awk -F',' 'FNR>1 {printf("%s|%f|%f|%f|%f|%f|%f|%f|%f|%f|%d %d\n"),$3, $4, $5, $7, $8, $15, $16, $17, $18, $19, $21, $1}' ${in_jiggle_file} | sed 's/ /T/' > ${tmp_file}
awk -F',' 'FNR>1 {printf("%s|%f|%f|%f|%f %d\n"),$3, $4, $5, $7, $8, $1}' ${in_jiggle_file} | sed 's/ /T/' > ${tmp_file}

# First column will have epoch time, then all others
###paste <(awk -F'|' '{system("gdate -d"$1" +%s")}' ${tmp_file}) <(awk -F'|' '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}' ${tmp_file}) > ${tmp2_file}
paste <(awk -F'|' '{system("gdate -d"$1" +%s.%N")}' ${tmp_file}) <(awk -F'|' '{print $1, $2, $3, $4, $5, $6}' ${tmp_file}) > ${tmp2_file}

# Remove catalog_start_time from epoch time
catalog_start_epoch=`gdate -d${catalog_start_time} +%s.%N`
echo ${catalog_start_epoch}

# Output plain text catalog with columns: time(sec) since catalog_start_time, orign time, latitude, longitude, depth, gap, dist, rms, errh, errz, obs, eventid
###awk -v ctime=${catalog_start_epoch} '{printf "%15.5f %s %8.5f %8.5f %6.4f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %d %d\n", $1-ctime, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13}' ${tmp2_file} > ${out_catalog_file}
awk -v ctime=${catalog_start_epoch} '{printf "%15.5f %s %8.5f %8.5f %6.4f %5.3f %d\n", $1-ctime, $2, $3, $4, $5, $6, $7}' ${tmp2_file} > ${out_catalog_file}
#rm ${tmp_file} ${tmp2_file}

