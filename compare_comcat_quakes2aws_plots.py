from obspy.geodetics.base import gps2dist_azimuth
import datetime
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams.update({'font.size': 24})
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"

# Plot catalog vs quakes2AWS
def plot_compare_vs_catalog(cat_arr, q2a_arr, size_plot_arr, x_min, x_max, y_min, y_max, x_ticks, y_ticks, x_label, y_label, out_file):
#   size_plot_arr = np.multiply(size_arr, 10)
   equal_line = np.linspace(x_min, x_max, 100)
   plt.figure(num=0, figsize=(6,6))
   plt.clf()
   plt.scatter(cat_arr, q2a_arr, size_plot_arr, facecolors='none', edgecolors='k', linewidths=0.5)
   plt.plot(equal_line, equal_line, '--', color='k', linewidth=0.8)
   plt.axis('scaled')
   plt.xlim([x_min, x_max])
   plt.ylim([y_min, y_max])
   plt.xticks(x_ticks)
   plt.yticks(y_ticks)
   plt.xlabel(x_label)
   plt.ylabel(y_label)
   plt.tight_layout()
   plt.savefig(out_file)


# Plot (quakes2AWS-catalog) residual vs catalog
def plot_compare_residual_vs_catalog(cat_arr, res_arr, size_plot_arr, x_min, x_max, y_min, y_max, x_ticks, y_ticks, x_label, y_label, out_file):
#   size_plot_arr = np.multiply(size_arr, 10)
   equal_line = np.linspace(x_min, x_max, 100)
   zero_line = np.zeros(100)
   plt.figure(num=1, figsize=(6,6))
   plt.clf()
   plt.scatter(cat_arr, res_arr, size_plot_arr, facecolors='none', edgecolors='k', linewidths=0.5)
   plt.plot(equal_line, zero_line, '--', color='k', linewidth=0.8)
   plt.xlim([x_min, x_max])
   plt.ylim([y_min, y_max])
   plt.xticks(x_ticks)
   plt.yticks(y_ticks)
   plt.xlabel(x_label)
   plt.ylabel(y_label)
   plt.tight_layout()
   plt.savefig(out_file)



#in_match_file = '../data/20190704_73hr_events_MATCH_scedc_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time='2019-07-04T00:00:00'
#out_dir = '../figs/'
#out_str = '20190704_73hr_MATCH_scedc_gpdbinder'

in_match_file = '../data/20190704_73hr_events_MATCH_pickew_gpdbinder.txt'
alg1_str = 'pickEW'
alg2_str = 'GPD-binder'
catalog_start_time='2019-07-04T00:00:00'
out_dir = '../figs/'
out_str = '20190704_73hr_MATCH_pickew_gpdbinder'

#in_match_file = '../data/20200810_4hr_events_MATCH_comcat_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time = '2020-08-10T00:00:00'
#out_dir = '../figs/'
#out_str = '20200810_4hr_MATCH_comcat_gpdbinder'

#in_match_file = '../data/20200930_1545hr_events_MATCH_comcat_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../figs/'
#out_str = '20200930_1545hr_MATCH_comcat_gpdbinder'

#in_match_file = '../data/20200930_1545hr_events_MATCH_comcat_pickew.txt'
#alg1_str = 'Catalog'
#alg2_str = 'pickEW'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../figs/'
#out_str = '20200930_1545hr_MATCH_comcat_pickew'

#in_match_file = '../data/20200930_1545hr_events_MATCH_pickew_gpdbinder.txt'
#alg1_str = 'pickEW'
#alg2_str = 'GPD-binder'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../figs/'
#out_str = '20200930_1545hr_MATCH_pickew_gpdbinder'

#in_match_file = '../data/20200930_24hr_events_MATCH_scedc_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../figs/'
#out_str = '20200930_24hr_MATCH_scedc_gpdbinder'

#in_match_file = '../data/20200930_24hr_events_MATCH_pickew_gpdbinder.txt'
#alg1_str = 'pickEW'
#alg2_str = 'GPD-binder'
#catalog_start_time = '2020-09-30T00:00:00'
#out_dir = '../figs/'
#out_str = '20200930_24hr_MATCH_pickew_gpdbinder'

#in_match_file = '../data/20210104_24hr_events_MATCH_scedc_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time='2021-01-04T00:00:00'
#out_dir = '../figs/'
#out_str = '20210104_24hr_MATCH_scedc_gpdbinder'

#in_match_file = '../data/20210104_24hr_events_MATCH_pickew_gpdbinder.txt'
#alg1_str = 'pickEW'
#alg2_str = 'GPD-binder'
#catalog_start_time='2021-01-04T00:00:00'
#out_dir = '../figs/'
#out_str = '20210104_24hr_MATCH_pickew_gpdbinder'

#in_match_file = '../data/20210605_24hr_events_MATCH_scedc_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time='2021-06-05T00:00:00'
#out_dir = '../figs/'
#out_str = '20210605_24hr_MATCH_scedc_gpdbinder'

#in_match_file = '../data/20210605_24hr_events_MATCH_pickew_gpdbinder.txt'
#alg1_str = 'pickEW'
#alg2_str = 'GPD-binder'
#catalog_start_time='2021-06-05T00:00:00'
#out_dir = '../figs/'
#out_str = '20210605_24hr_MATCH_pickew_gpdbinder'

#in_match_file = '../data/20220325_25hr_events_MATCH_scedc_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time = '2022-03-25T18:00:00'
#out_dir = '../figs/'
#out_str = '20220325_25hr_MATCH_scedc_gpdbinder'

#in_match_file = '../data/20220325_25hr_events_MATCH_pickew_gpdbinder.txt'
#alg1_str = 'pickEW'
#alg2_str = 'GPD-binder'
#catalog_start_time = '2022-03-25T18:00:00'
#out_dir = '../figs/'
#out_str = '20220325_25hr_MATCH_pickew_gpdbinder'

#in_match_file = '../data/20220325R_25hr_events_MATCH_replay_gpdbinder.txt'
#alg1_str = 'GPD-binder realtime'
#alg2_str = 'GPD-binder replay'
#catalog_start_time = '2022-03-25T18:00:00'
#out_dir = '../figs/'
#out_str = '20220325R_25hr_MATCH_replay_gpdbinder'

#in_match_file = '../data/20220408_4d_events_MATCH_scedc_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time='2022-04-08T00:30:00'
#out_dir = '../figs/'
#out_str = '20220408_4d_MATCH_scedc_gpdbinder'

#in_match_file = '../data/20220408_4d_events_MATCH_pickew_gpdbinder.txt'
#alg1_str = 'pickEW'
#alg2_str = 'GPD-binder'
#catalog_start_time='2022-04-08T00:30:00'
#out_dir = '../figs/'
#out_str = '20220408_4d_MATCH_pickew_gpdbinder'

#in_match_file = '../data/20220421_8d_events_MATCH_scedc_gpdbinder.txt'
#alg1_str = 'Catalog'
#alg2_str = 'GPD-binder'
#catalog_start_time='2022-04-21T23:16:00'
#out_dir = '../figs/'
#out_str = '20220421_8d_MATCH_scedc_gpdbinder'

#in_match_file = '../data/20220421_8d_events_MATCH_pickew_gpdbinder.txt'
#alg1_str = 'pickEW'
#alg2_str = 'GPD-binder'
#catalog_start_time='2022-04-21T23:16:00'
#out_dir = '../figs/'
#out_str = '20220421_8d_MATCH_pickew_gpdbinder'

q2a_arr_ot = []
q2a_arr_lat = []
q2a_arr_lon = []
q2a_arr_depth = []
q2a_arr_mag = []
q2a_arr_evid = []

cat_arr_ot = []
cat_arr_lat = []
cat_arr_lon = []
cat_arr_depth = []
cat_arr_mag = []
cat_arr_evid = []

epidist_q2a_cat = []
hypodist_q2a_cat = []
diff_arr_ot = []
diff_arr_lat = []
diff_arr_lon = []
diff_arr_depth = []
diff_arr_mag = []

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
with open(in_match_file, 'r') as fin:
   for line in fin:
      split_line = line.split()

      q2a_ot = (datetime.datetime.strptime(split_line[1], "%Y-%m-%dT%H:%M:%S.%f") - catalog_ref_time).total_seconds()
      q2a_lat = float(split_line[2])
      q2a_lon = float(split_line[3])
      q2a_depth = float(split_line[4])
      q2a_mag = float(split_line[5])
      q2a_evid = split_line[6]

#      cat_ot = (datetime.datetime.strptime(split_line[7], "%Y-%m-%dT%H:%M:%S.%f") - catalog_ref_time).total_seconds()
      cat_ot = (datetime.datetime.strptime(split_line[7], "%Y/%m/%dT%H:%M:%S.%f") - catalog_ref_time).total_seconds()
      cat_lat = float(split_line[8])
      cat_lon = float(split_line[9])
      cat_depth = float(split_line[10])
      cat_mag = float(split_line[11])
      cat_evid = split_line[12]

      q2a_arr_ot.append(q2a_ot)
      q2a_arr_lat.append(q2a_lat)
      q2a_arr_lon.append(q2a_lon)
      q2a_arr_depth.append(q2a_depth)
      q2a_arr_mag.append(q2a_mag)
      q2a_arr_evid.append(q2a_evid)

      cat_arr_ot.append(cat_ot)
      cat_arr_lat.append(cat_lat)
      cat_arr_lon.append(cat_lon)
      cat_arr_depth.append(cat_depth)
      cat_arr_mag.append(cat_mag)
      cat_arr_evid.append(cat_evid)

#      diff_arr_ot.append((q2a_ot - cat_ot).total_seconds())
      diff_arr_ot.append(q2a_ot - cat_ot)
      diff_arr_lat.append(q2a_lat - cat_lat)
      diff_arr_lon.append(q2a_lon - cat_lon)
      diff_arr_depth.append(q2a_depth - cat_depth)
      diff_arr_mag.append(q2a_mag - cat_mag)

      [epi_dist, azAB, azBA] = gps2dist_azimuth(q2a_lat, q2a_lon, cat_lat, cat_lon)
      epidist_q2a_cat.append(0.001*epi_dist) #km
      hypo_dist = math.sqrt((0.001*epi_dist)**2 + (q2a_depth-cat_depth)**2)
      hypodist_q2a_cat.append(hypo_dist) #km


size_plot_depth_sq = np.multiply(diff_arr_depth, diff_arr_depth)
#size_plot_dist_arr = np.multiply(hypodist_q2a_cat, 15)
size_plot_dist_arr = np.multiply(size_plot_depth_sq, 10)
size_plot_mag_sq = np.multiply(cat_arr_mag, cat_arr_mag)
size_plot_mag_arr = np.multiply(size_plot_mag_sq, 20)

# Compare magnitudes - sized by hypocentral distance
#plot_compare_vs_catalog(cat_arr_mag, q2a_arr_mag, size_plot_dist_arr, -0.5, 4.5, -0.5, 4.5, 
#   [0, 1, 2, 3, 4], [0, 1, 2, 3, 4],
#plot_compare_vs_catalog(cat_arr_mag, q2a_arr_mag, size_plot_dist_arr, -0.5, 5.5, -0.5, 5.5, 
#   [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5],
plot_compare_vs_catalog(cat_arr_mag, q2a_arr_mag, size_plot_dist_arr, -0.5, 8.5, -0.5, 8.5, 
   [0, 2, 4, 6, 8], [0, 2, 4, 6, 8],
   alg1_str+' magnitude', alg2_str+' magnitude',
   out_dir+out_str+'_magnitude_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_mag, diff_arr_mag, size_plot_dist_arr, -0.5, 4.5, -2, 2, 
#   [0, 1, 2, 3, 4], [-2, -1, 0, 1, 2],
#plot_compare_residual_vs_catalog(cat_arr_mag, diff_arr_mag, size_plot_dist_arr, -0.5, 5.5, -2, 2, 
#   [0, 1, 2, 3, 4, 5], [-2, -1, 0, 1, 2],
plot_compare_residual_vs_catalog(cat_arr_mag, diff_arr_mag, size_plot_dist_arr, -0.5, 8.5, -2, 2, 
   [0, 2, 4, 6, 8], [-2, -1, 0, 1, 2],
   alg1_str+' magnitude', 'Residual magnitude:\n['+alg2_str+' - '+alg1_str,
   out_dir+out_str+'_magnitude_residual.pdf')

# Compare latitudes - sized by magnitude
#plot_compare_vs_catalog(cat_arr_lat, q2a_arr_lat, 33, 37, 33, 37,
#   [33, 34, 35, 36, 37], [33, 34, 35, 36, 37],
#plot_compare_vs_catalog(cat_arr_lat, q2a_arr_lat, 33, 34, 33, 34,
#   [33, 33.5, 34], [33, 33.5, 34],
plot_compare_vs_catalog(cat_arr_lat, q2a_arr_lat, size_plot_mag_arr, 31.5, 37.5, 31.5, 37.5,
   [32, 33, 34, 35, 36, 37], [32, 33, 34, 35, 36, 37],
   alg1_str+' latitude (deg)', alg2_str+' latitude (deg)',
   out_dir+out_str+'_latitude_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_lat, diff_arr_lat, 33, 37, -0.1, 0.1,
#   [33, 34, 35, 36, 37], [-0.1, -0.05, 0, 0.05, 0.1],
#plot_compare_residual_vs_catalog(cat_arr_lat, diff_arr_lat, 33, 34, -0.1, 0.1,
#   [33, 33.5, 34], [-0.1, -0.05, 0, 0.05, 0.1],
#plot_compare_residual_vs_catalog(cat_arr_lat, diff_arr_lat, size_plot_mag_arr, 31.5, 37.5, -0.15, 0.15,
#   [32, 33, 34, 35, 36, 37], [-0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15],
plot_compare_residual_vs_catalog(cat_arr_lat, diff_arr_lat, size_plot_mag_arr, 31.5, 37.5, -0.3, 0.3,
   [32, 33, 34, 35, 36, 37], [-0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3],
   alg1_str+' latitude (deg)', 'Residual latitude (deg):\n['+alg2_str+' - '+alg1_str,
   out_dir+out_str+'_latitude_residual.pdf')

# Compare longitudes - sized by magnitude
#plot_compare_vs_catalog(cat_arr_lon, q2a_arr_lon, -120, -115, -120, -115,
plot_compare_vs_catalog(cat_arr_lon, q2a_arr_lon, size_plot_mag_arr, -121.5, -114.5, -121.5, -114.5,
   [-121, -119, -117, -115], [-121, -119, -117, -115],
   alg1_str+' longitude (deg)', alg2_str+' longitude (deg)',
   out_dir+out_str+'_longitude_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_lon, diff_arr_lon, -120, -115, -0.1, 0.1,
#plot_compare_residual_vs_catalog(cat_arr_lon, diff_arr_lon, size_plot_mag_arr, -121.5, -114.5, -0.15, 0.15,
#   [-121, -119, -117, -115], [-0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15],
plot_compare_residual_vs_catalog(cat_arr_lon, diff_arr_lon, size_plot_mag_arr, -121.5, -114.5, -0.3, 0.3,
   [-121, -119, -117, -115], [-0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3],
   alg1_str+' longitude (deg)', 'Residual longitude (deg):\n['+alg2_str+' - '+alg1_str,
   out_dir+out_str+'_longitude_residual.pdf')

# Compare depths - sized by magnitude
plot_compare_vs_catalog(cat_arr_depth, q2a_arr_depth, size_plot_mag_arr, -5, 20, -5, 20,
   [-5, 0, 5, 10, 15, 20], [-5, 0, 5, 10, 15, 20],
#plot_compare_vs_catalog(cat_arr_depth, q2a_arr_depth, size_plot_mag_arr, -5, 50, -5, 50,
#   [0, 10, 20, 30, 40, 50], [0, 10, 20, 30, 40, 50],
   alg1_str+' depth (km)', alg2_str+' depth (km)',
   out_dir+out_str+'_depth_compare.pdf')
plot_compare_residual_vs_catalog(cat_arr_depth, diff_arr_depth, size_plot_mag_arr, -5, 20, -20, 20,
   [-5, 0, 5, 10, 15, 20], [-20, -10, 0, 10, 20],
#plot_compare_residual_vs_catalog(cat_arr_depth, diff_arr_depth, size_plot_mag_arr, -5, 50, -40, 40,
#   [0, 10, 20, 30, 40, 50], [-40, -20, 0, 20, 40],
   alg1_str+' depth (km)', 'Residual depth (km):\n['+alg2_str+' - '+alg1_str,
   out_dir+out_str+'_depth_residual.pdf')

# Compare origin times
#plot_compare_vs_catalog(cat_arr_ot, q2a_arr_ot, size_plot_mag_arr, 0, 90000, 0, 90000,
plot_compare_vs_catalog(cat_arr_ot, q2a_arr_ot, size_plot_mag_arr, 0, 300000, 0, 300000,
#plot_compare_vs_catalog(cat_arr_ot, q2a_arr_ot, size_plot_mag_arr, 0, 750000, 0, 750000,
#   [0, 19200, 38400, 57600], [0, 19200, 38400, 57600],
#   [0, 30000, 60000, 90000], [0, 30000, 60000, 90000],
   [0, 100000, 200000, 300000], [0, 100000, 200000, 300000],
#   [0, 200000, 400000], [0, 200000, 400000],
#   [0, 250000, 500000, 750000], [0, 250000, 500000, 750000],
   alg1_str+' origin time (s)', alg2_str+' origin time (s)',
   out_dir+out_str+'_origintime_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_ot, diff_arr_ot, size_plot_mag_arr, 0, 90000, -4, 4,
plot_compare_residual_vs_catalog(cat_arr_ot, diff_arr_ot, size_plot_mag_arr, 0, 300000, -4, 4,
#plot_compare_residual_vs_catalog(cat_arr_ot, diff_arr_ot, size_plot_mag_arr, 0, 750000, -4, 4,
#   [0, 19200, 38400, 57600], [-4, -2, 0, 2, 4],
#   [0, 30000, 60000, 90000], [-4, -2, 0, 2, 4],
   [0, 100000, 200000, 300000], [-4, -2, 0, 2, 4],
#   [0, 200000, 400000], [-4, -2, 0, 2, 4],
#   [0, 250000, 500000, 750000], [-4, -2, 0, 2, 4],
   alg1_str+' origin time (s)', 'Residual origin time (s):\n['+alg2_str+' - '+alg1_str,
   out_dir+out_str+'_origintime_residual.pdf')

## Compare origin times
#plot_compare_vs_catalog(cat_arr_ot, q2a_arr_ot, 72000, 87000, 72000, 87000,
#   [72000, 77000, 82000, 87000], [72000, 77000, 82000, 87000],
#   alg1_str+' origin time (s)', alg2_str+' origin time (s)',
#   out_dir+out_str+'_origintime_compare.pdf')
#plot_compare_residual_vs_catalog(cat_arr_ot, diff_arr_ot, 72000, 87000, -4, 4,
#   [72000, 77000, 82000, 87000], [-4, -2, 0, 2, 4],
#   alg1_str+' origin time (s)', 'Residual origin time (s):\n['+alg2_str+' - '+alg1_str,
#   out_dir+out_str+'_origintime_residual.pdf')

## Compare catalog origin time vs depth - tradeoff?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(cat_arr_ot, cat_arr_depth, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([0, 57600])
##plt.xlim([72000, 87000])
#plt.ylim([0, 20])
#plt.xticks([0, 19200, 38400, 57600])
##plt.xticks([72000, 77000, 82000, 87000])
#plt.yticks([0, 5, 10, 15, 20])
#plt.xlabel(alg1_str+' origin time (s)')
#plt.ylabel(alg1_str+' depth (km)')
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_MATCH_comcat_view_origintime_vs_depth.pdf')
#
## Compare gpdbinder origin time vs depth - tradeoff?
#plt.figure(num=2, figsize=(6,6))
#plt.clf()
#plt.scatter(q2a_arr_ot, q2a_arr_depth, facecolors='none', edgecolors='k', linewidths=0.5)
#plt.xlim([0, 57600])
##plt.xlim([72000, 87000])
#plt.ylim([0, 20])
#plt.xticks([0, 19200, 38400, 57600])
##plt.xticks([72000, 77000, 82000, 87000])
#plt.yticks([0, 5, 10, 15, 20])
#plt.xlabel(alg2_str+' origin time (s)')
#plt.ylabel(alg2_str+' depth (km)')
#plt.tight_layout()
#plt.savefig(out_dir+out_str+'_MATCH_gpdbinder_view_origintime_vs_depth.pdf')

# Compare residuals - origin time vs depth - tradeoff?
plt.figure(num=2, figsize=(6,6))
plt.clf()
plt.scatter(diff_arr_ot, diff_arr_depth, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
plt.xlim([-4, 4])
plt.ylim([-40, 40])
plt.xticks([-4, -2, 0, 2, 4])
plt.yticks([-40, -20, 0, 20, 40])
plt.xlabel('Residual origin time (s):\n['+alg2_str+' - '+alg1_str)
plt.ylabel('Residual depth (km):\n['+alg2_str+' - '+alg1_str)
plt.tight_layout()
plt.savefig(out_dir+out_str+'_residual_origintime_vs_depth.pdf')

# Compare residuals - origin time vs epidist - tradeoff?
plt.figure(num=2, figsize=(6,6))
plt.clf()
plt.scatter(diff_arr_ot, epidist_q2a_cat, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
plt.xlim([-4, 4])
plt.ylim([0, 20])
plt.xticks([-4, -2, 0, 2, 4])
plt.yticks([0, 10, 20])
plt.xlabel('Residual origin time (s):\n['+alg2_str+' - '+alg1_str)
plt.ylabel('Epicentral distance (km):\n['+alg2_str+' - '+alg1_str)
plt.tight_layout()
plt.savefig(out_dir+out_str+'_residual_origintime_vs_epidist.pdf')

# Compare residuals - origin time vs hypodist - tradeoff?
plt.figure(num=2, figsize=(6,6))
plt.clf()
plt.scatter(diff_arr_ot, hypodist_q2a_cat, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
plt.xlim([-4, 4])
plt.ylim([0, 60])
plt.xticks([-4, -2, 0, 2, 4])
plt.yticks([0, 30, 60])
plt.xlabel('Residual origin time (s):\n['+alg2_str+' - '+alg1_str)
plt.ylabel('Epicentral distance (km):\n['+alg2_str+' - '+alg1_str)
plt.tight_layout()
plt.savefig(out_dir+out_str+'_residual_origintime_vs_hypodist.pdf')

# Compare depth vs residual magnitude - correlation?
plt.figure(num=2, figsize=(6,6))
plt.clf()
plt.scatter(cat_arr_depth, diff_arr_mag, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
plt.xlim([-5, 20])
plt.ylim([-2, 2])
plt.xticks([-5, 0, 5, 10, 15, 20])
plt.yticks([-2, -1, 0, 1, 2])
plt.xlabel('Catalog depth (km)')
plt.ylabel('Residual magnitude:\n['+alg2_str+' - '+alg1_str+']')
plt.tight_layout()
plt.savefig(out_dir+out_str+'_depth_vs_residual_magnitude.pdf')

# Compare residual depth vs residual magnitude - correlation?
plt.figure(num=2, figsize=(6,6))
plt.clf()
plt.scatter(diff_arr_depth, diff_arr_mag, size_plot_mag_arr, facecolors='none', edgecolors='k', linewidths=0.5)
plt.xlim([-20, 20])
plt.ylim([-2, 2])
plt.xticks([-20, -10, 0, 10, 20])
plt.yticks([-2, -1, 0, 1, 2])
plt.xlabel('Residual depth (km):\n['+alg2_str+' - '+alg1_str+']')
plt.ylabel('Residual magnitude:\n['+alg2_str+' - '+alg1_str+']')
plt.tight_layout()
plt.savefig(out_dir+out_str+'_residual_depth_vs_residual_magnitude.pdf')

