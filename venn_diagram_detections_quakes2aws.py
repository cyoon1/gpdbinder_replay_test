import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib_venn import venn2, venn2_circles # conda install matplotlib-venn

rcParams.update({'font.size': 14})
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"


#catalog_str = 'Comcat-SCEDC'
#catalt_str = 'GPD-Binder'
#data_str = '20200810_4hr Bombay Beach'
#num_missed = 24
#num_new = 67
#num_match = 63
#out_file = '../figs/20200810_4hr_venn_diagram_detections.pdf'

#catalog_str = 'Comcat-SCEDC'
#catalt_str = 'GPD-Binder'
#data_str = '20200930_1545hr Westmorland'
#num_missed = 16
#num_new = 93
#num_match = 61
#out_file = '../figs/20200930_1545hr_venn_diagram_detections_comcat_gpdbinder.pdf'

#catalog_str = 'Comcat-SCEDC'
#catalt_str = 'AQMS PickEW'
#data_str = '20200930_1545hr Westmorland'
#num_missed = 23
#num_new = 1
#num_match = 54
#out_file = '../figs/20200930_1545hr_venn_diagram_detections_comcat_pickew.pdf'

#catalog_str = 'AQMS PickEW'
#catalt_str = 'GPD-Binder'
#data_str = '20200930_1545hr Westmorland'
#num_missed = 3
#num_new = 102
#num_match = 52
#out_file = '../figs/20200930_1545hr_venn_diagram_detections_pickew_gpdbinder.pdf'

#catalog_str = 'Comcat-SCEDC'
#catalt_str = 'GPD-Binder'
#data_str = '20220319_1015hr RealTime'
#num_missed = 1
#num_new = 13
#num_match = 7
#out_file = '../figs/20220319_1015hr_venn_diagram_detections_comcat_gpdbinder.pdf'

catalog_str = 'Comcat-SCEDC'
catalt_str = 'GPD-Binder'
data_str = '20220325_25hr RealTime'
num_missed = 14
num_new = 66
num_match = 28
out_file = '../figs/20220325_25hr_venn_diagram_detections_comcat_gpdbinder.pdf'




num_catalog = num_match + num_missed
num_catalt = num_match + num_new
num_total = num_match + num_missed + num_new

# Subset sizes
s = (
      float(num_missed)/num_total,  # Ab
      float(num_new)/num_total,  # aB
      float(num_match)/num_total,  # AB
)

v = venn2(subsets=s, set_labels=(catalog_str+':\n '+str(num_catalog)+' events', catalt_str+':\n '+str(num_catalt)+' events'))

# Subset labels
v.get_label_by_id('10').set_text(str(num_missed)+'\n'+catalog_str+'\nonly\nevents')
v.get_label_by_id('01').set_text(str(num_new)+'\n'+catalt_str+'\nonly\nevents')
v.get_label_by_id('11').set_text(str(num_match)+'\noverlapping\nevents')

# Subset colors
v.get_patch_by_id('10').set_color('red')
v.get_patch_by_id('01').set_color('cyan')
v.get_patch_by_id('11').set_color('blue')

# Subset alphas
v.get_patch_by_id('10').set_alpha(0.4)
v.get_patch_by_id('01').set_alpha(1.0)
v.get_patch_by_id('11').set_alpha(0.7)

# Border styles
c = venn2_circles(subsets=s, linestyle='solid')
#c[0].set_ls('dashed')  # Line style
#c[0].set_lw(2.0)       # Line width

plt.title('Total: '+str(num_total)+' earthquakes, '+data_str)
plt.savefig(out_file)

