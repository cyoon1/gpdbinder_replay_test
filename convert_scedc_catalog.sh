#!/bin/bash

# Convert catalog in USGS ComCat API format to a plain text catalog
#
# 2020-11-02	C. Yoon    First created

in_scedc_file='../data/scedc_20190704_73hr_events_alltypes.txt'
out_catalog_file='../data/20190704_73hr_scedc_event_catalog.txt'
catalog_start_time='2019-07-04T00:00:00'

#in_scedc_file='../data/scedc_20210605_events_clean.txt'
#out_catalog_file='../data/20210605_scedc_event_catalog.txt'
#catalog_start_time='2021-06-05T00:00:00'

#in_scedc_file='../data/scedc_20200810_4hr_events_clean.txt'
#out_catalog_file='../data/20200810_4hr_scedc_event_catalog.txt'
#catalog_start_time='2020-08-10T00:00:00'

#in_scedc_file='../data/scedc_20200930_1545hr_events_clean.txt'
#out_catalog_file='../data/20200930_1545hr_scedc_event_catalog.txt'
#catalog_start_time='2020-09-30T00:00:00'

#in_scedc_file='../data/scedc_20200930_24hr_events_alltypes.txt'
#out_catalog_file='../data/20200930_24hr_scedc_event_catalog.txt'
#catalog_start_time='2020-09-30T00:00:00'

#in_scedc_file='../data/scedc_20210104_24hr_events_alltypes.txt'
#out_catalog_file='../data/20210104_24hr_scedc_event_catalog.txt'
#catalog_start_time='2021-01-04T00:00:00'

#in_scedc_file='../data/scedc_20210605_24hr_events_alltypes.txt'
#out_catalog_file='../data/20210605_24hr_scedc_event_catalog.txt'
#catalog_start_time='2021-06-05T00:00:00'

#in_scedc_file='../data/scedc_20220325_25hr_events_alltypes.txt'
#out_catalog_file='../data/20220325_25hr_scedc_event_catalog.txt'
#catalog_start_time='2022-03-25T18:00:00'

#in_scedc_file='../data/scedc_20220408_4d_events_alltypes.txt'
#out_catalog_file='../data/20220408_4d_scedc_event_catalog.txt'
#catalog_start_time='2022-04-08T00:30:00'

#in_scedc_file='../data/scedc_20220421_8d_events_alltypes.txt'
#out_catalog_file='../data/20220421_8d_scedc_event_catalog.txt'
#catalog_start_time='2022-04-21T23:16:00'

tmp_file=tmp.txt
tmp2_file=tmp2.txt
#awk '{printf("%sT%s %f %f %f %f %d %d\n"), $1, $2, $7, $8, $9, $5, $12, $11}' ${in_scedc_file} > ${tmp_file}
awk '{printf("%sT%s %f %f %f %f %d\n"), $1, $2, $7, $8, $9, $5, $11}' ${in_scedc_file} > ${tmp_file}

# First column will have epoch time, then all others
#paste <(awk '{system("gdate -d"$1" +%s")}' ${tmp_file}) <(awk '{print $1, $2, $3, $4, $5, $6, $7, $8}' ${tmp_file}) > ${tmp2_file}
paste <(awk '{system("gdate -d"$1" +%s.%N")}' ${tmp_file}) <(awk '{print $1, $2, $3, $4, $5, $6, $7}' ${tmp_file}) > ${tmp2_file}

# Remove catalog_start_time from epoch time
catalog_start_epoch=`gdate -d${catalog_start_time} +%s.%N`
#echo ${catalog_start_epoch}

# Output plain text catalog with columns: time(sec) since catalog_start_time, orign time, latitude, longitude, depth, magnitude, obs, eventid
#awk -v ctime=${catalog_start_epoch} '{printf "%15.5f %s %8.5f %8.5f %6.4f %5.3f %d %d\n", $1-ctime, $2, $3, $4, $5, $6, $7, $8}' ${tmp2_file} > ${out_catalog_file}
awk -v ctime=${catalog_start_epoch} '{printf "%15.5f %s %8.5f %8.5f %6.4f %5.3f %d\n", $1-ctime, $2, $3, $4, $5, $6, $7}' ${tmp2_file} > ${out_catalog_file}
rm ${tmp_file} ${tmp2_file}

