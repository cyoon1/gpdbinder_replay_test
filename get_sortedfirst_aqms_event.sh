#!/bin/bash

# Script to get first AQMS automatic event
# Runs on amosite

# get_sortedfirst_aqms_event.sh
if [[ $# -ne 2 ]] ; then
   echo 'Usage: ./get_sortedfirst_aqms_event.sh <eventid> <input_str>'
   exit 1
fi
#echo $1
#echo "$2"

# Get first aqms event (real-time, binder, original event solution)
#eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | sort -t',' -nrk1,3 | awk '{$1=""; print $0}' | tail -n 1
#eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | sort -t',' -nrk1,3 | tail -n 1
#eventhist -d archdbe $1 | tail -n +3 | awk '{print $NF,$0}' | tail -n 1
#eventhist -d archdbe $1 | tail -n +3 | awk '/A  RT/ {print $NF,$0}' | sort -t',' -nrk1,3 | tail -n 1

ORID=`eventhist -d archdbe $1 | tail -n +3 | awk '"$2" {print $NF,$0}' | tail -n 1 | awk -F, '{print $2}'`
eventhist -d archdbe $1 | tail -n +3 | awk '"$2" {print $NF,$0}' | grep ${ORID} | sort -t',' -nrk1,3 | tail -n 1
